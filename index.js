//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

const to = require('await-to-js').default;
const nedb = require('nedb');
const express = require('express');
const bodyparser = require('body-parser');
const app = express();
const promisify = require('util').promisify;

const PORT = 9011;
const EXPIRY_HOURS = 5;

app.use(bodyparser.json());

const start = async ()=>{
  const db = new nedb({ filename: 'serverlist.db'});
  await db.loadDatabase();

  db.find = promisify(db.find);
  db.update = promisify(db.update);
  db.insert = promisify(db.insert);
  db.remove = promisify(db.remove);

  //
  // --------------------------------------------------------------------------
  //
  
  app.get('/server', async (req,res)=>{
    const now = new Date(Date.now());
    const [err1, count] = await to(db.remove({exp: { $lte: now } }));

    const [err2, docs] = await to(db.find({_id: {$ne: false}}));
    const games = docs.map((item)=>{
      const result = item;
      delete result._id;
      return result;
    });

    console.log(games);
    res.json({games});
  });

  //
  // --------------------------------------------------------------------------
  //

  app.post('/server', async (req,res)=>{
    const reject = (err)=>{
      console.error(err);
      res.send(err);
    }

    if(req.body && req.body.ip && req.body.port && req.body.name) {

      let exp = new Date(Date.now());
      exp.setHours(exp.getHours() + EXPIRY_HOURS);
      let doc = {
        ip: req.body.ip,
        port: req.body.port,
        name: req.body.name,
        exp
      };

      const [err1, docs] = await to(db.find({ip: req.body.ip, port: req.body.port}));
      if(err1) return reject(err1);
      if(docs && docs.length > 0) {
        const [err2] = await to(db.update({_id: docs[0]._id}, doc));
        if(err2) return reject(err2);
        return res.json(doc);
      }
      else {
        const [err2, newdoc] = await to(db.insert(doc));
        if(err2) return reject(err2);
        return res.json(newdoc);
      }
    }    

    return res.json({err: 'invalid game settings'});
  });

  //
  // --------------------------------------------------------------------------
  //

  app.delete('/server', async (req, res)=>{
    if(req.body && req.body.id) {
      const [err, num] = await to(db.remove({_id: req.body.id}));
      res.json({err, num});
    }

    return res.json({err: 'invalid server'});
  });

  //
  // //////////////////////////////////////////////////////////////////////////
  //

  app.listen(PORT, ()=>{
    console.log('ready');
  });
}

start();
